import os
import random

from jacolib import assistant

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
assist: assistant.Assistant
jokes: list


# ==================================================================================================


def load_jokes(lang, safe_only=False):
    file_path = os.path.dirname(os.path.realpath(__file__)) + "/"

    if safe_only:
        path = file_path + "jokes/" + lang.lower() + "_safe_only.txt"
    else:
        path = file_path + "jokes/" + lang.lower() + ".txt"

    with open(path, encoding="utf-8") as file:
        # Read line by line and remove whitespace characters at the end of each line
        content = file.readlines()
        content = [x.strip() for x in content]

    random.shuffle(content)
    return content


# ==================================================================================================


def get_joke():
    """Get first entry and append it to the end -> no repetition until every entry selected"""

    joke = jokes.pop(0)
    jokes.append(joke)
    return joke


# ==================================================================================================


def callback_tell_joke(message):
    result_sentence = get_joke()
    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def main():
    global assist, jokes

    assist = assistant.Assistant(repo_path=file_path)
    jokes = load_jokes(
        assist.get_global_config()["language"],
        assist.get_config()["user"]["safe_jokes_only"] == "true",
    )

    assist.add_topic_callback("tell_joke", callback_tell_joke)
    assist.run()


# ==================================================================================================

if __name__ == "__main__":
    main()
